#!/usr/bin/env bash

SRC_DIR=$(dirname $0)

. "${SRC_DIR}"/env.fn.sh
. "${SRC_DIR}"/term.fn.sh
. "${SRC_DIR}"/conf.fn.sh

TEST_CONF="${SRC_DIR}"/conf.example

echo "Sections:"
pkg_get_sections "${TEST_CONF}"

echo "Packages in base section:"
pkg_query_section "${TEST_CONF}" base
