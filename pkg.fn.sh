
DISTRO_ID=""

pkg_get_conf()
{
	local _confdir

	_confdir=$1
	echo "${_confdir}/${KERNEL_NAME}/${DISTRO_ID}"
}

pkg_update_info()
{
	return
}

pkg_upgrade()
{
	return
}

pkg_install()
{
	local _pkgname

	_pkgname=$1
	return
}

pkg_installed_version()
{
	local _pkgname

	_pkgname=$1
	return
}

pkg_target_version()
{
	local _pkgname

	_pkgname=$1
	return
}

KERNEL_NAME=$(uname -s)
m_info "Detected kernel: " '\+bold' "${KERNEL_NAME}" '\-bold'

if [ ! -d "${SRC_DIR}/${KERNEL_NAME}" ]
then
	m_error "Unsupported kernel: " '\+bold' "${KERNEL_NAME}" '\-bold'
	exit 1
fi
. "${SRC_DIR}/${KERNEL_NAME}"/fn.sh
