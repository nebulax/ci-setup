#!/usr/bin/env bash

SRC_DIR=$(dirname $0)

. "${SRC_DIR}"/env.fn.sh
. "${SRC_DIR}"/term.fn.sh

_msg_print "test1: msg"

_msg_print "test2: msg1" " and msg2 after space"

_msg_print "test3:" '\n' "msg after newline"

_msg_print "test4: " '\\+' " msg after \+"

_msg_print "test5: " '\+red' "msg in red" '\+reset'

_msg_print "test6: " '\+red' "redonly" '\+bold' "redbold" '\-bold' "redonly" '\+reset'
