
pkg_update_info()
{
	apt-get update
}

pkg_upgrade()
{
	apt-get -y upgrade
}

pkg_install()
{
	local _pkgname

	_pkgname=$1

	apt-get -y install ${_pkgname}
}

pkg_installed_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(dpkg-query -W -f '${Version}' ${_pkgname} 2>/dev/null || :)

	echo ${_version}
}

pkg_target_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(apt-cache show --no-all-versions ${_pkgname} | awk '$1 == "Version:" {print $2}')

	echo ${_version}
}
