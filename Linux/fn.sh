
. /etc/os-release
DISTRO_ID=${ID}

DISTRO_FUNCTIONS="${SRC_DIR}/${KERNEL_NAME}/${DISTRO_ID}.fn.sh"
if [ ! -e "${DISTRO_FUNCTIONS}" ]
then
	m_error "Unsupported ${KERNEL_NAME} distro: " '\+bold' "${DISTRO_ID}" '\-bold'
	exit 1
fi
. "${DISTRO_FUNCTIONS}"
